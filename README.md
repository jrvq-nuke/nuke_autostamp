# Nuke auto-stamp group

This executable script generates an auto-stamp node inside Nuke, displaying the basic information of a shot inside a simple pipeline.
The node is a group, which contains multiple text nodes inside itself. The size and position of the texts get adapted at all times to the dimensions of the input image, but its best performance is achieved with 16:9 ratio input images.

Example of stamping:
![stamp_example](http://www.jaimervq.com/wp-content/uploads/2020/01/stamp_example.jpg)


This script has been tested successfully in **Nuke 11.1v1.**
***

For more info: www.jaimervq.com
